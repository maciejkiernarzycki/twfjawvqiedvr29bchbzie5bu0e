package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"
	"url-collector/internal"
	"url-collector/pkg/collect"
	"url-collector/pkg/service"

	"github.com/gorilla/mux"
	"github.com/sethvargo/go-envconfig"
	"go.uber.org/zap"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}

	var config struct {
		Port               int    `env:"PORT,default=8080"`
		APIKey             string `env:"API_KEY,default=DEMO_KEY"`
		ConcurrentRequests int    `env:"CONCURRENT_REQUESTS,default=5"`
	}

	if err := envconfig.Process(context.Background(), &config); err != nil {
		logger.Fatal("invalid service configuration", zap.Error(err))
	}

	writer := internal.NewErrorWriter(logger)
	semaphore := internal.NewSemaphore(int64(config.ConcurrentRequests))

	var collector service.Collector

	collector = collect.NewNasaCollector(logger, config.APIKey)
	collector = collect.NewConcurrentCollector(logger, collector, semaphore)
	controller := service.NewPictureController(collector)
	picturesHandler := service.PictureHandler(controller, writer)

	router := mux.NewRouter()
	router.Use(LoggingMiddleware(logger))
	router.HandleFunc("/pictures", picturesHandler).Methods(http.MethodGet)

	server := http.Server{
		Addr:         fmt.Sprintf(":%d", config.Port),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		Handler:      router,
	}

	logger.Info("creating http server",
		zap.Int("port", config.Port),
		zap.Int("request_concurrency", config.ConcurrentRequests))

	go func() {
		err := server.ListenAndServe()
		if err == http.ErrServerClosed {
			return
		}

		logger.Warn("could not start http server", zap.Error(err))
		cancel()
	}()

	signals := make(chan os.Signal, 1)

	signal.Notify(signals, os.Interrupt, os.Kill)

	select {
	case <-signals:
	case <-ctx.Done():
	}

	logger.Info("shutting down service gracefully")

	signal.Stop(signals)

	if err := server.Shutdown(context.Background()); err != nil {
		logger.Error("could not gracefully shutdown http server", zap.Error(err))
	}

	_ = logger.Sync()

}

func LoggingMiddleware(logger *zap.Logger) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
			logger.Info("server received request",
				zap.String("uri", request.RequestURI),
				zap.String("method", request.Method))

			next.ServeHTTP(response, request)
		})
	}
}
