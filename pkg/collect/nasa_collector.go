package collect

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"url-collector/internal"
	"url-collector/pkg/service"

	"go.uber.org/zap"
)

const NasaDateFormat = "2006-01-02"

type NasaCollector struct {
	client *http.Client
	apiKey string
	logger *zap.Logger
}

func NewNasaCollector(logger *zap.Logger, apiKey string) *NasaCollector {
	return &NasaCollector{
		client: &http.Client{},
		apiKey: apiKey,
		logger: logger,
	}
}

func (nasa *NasaCollector) Collect(ctx context.Context, destination []string, options service.CollectorOptions) error {
	// NOTE: caching mechanism would be in tact, because queries are slow and there is 40 requests limit,
	//       or rethink system design from the ground and build more asynchronous solution.

	nasa.logger.Info("request sent to nasa api",
		zap.String("start_date", options.Start.String()),
		zap.String("end_date", options.End.String()),
	)

	url := url.URL{
		Scheme: "https",
		Host:   "api.nasa.gov",
		Path:   "/planetary/apod",
		RawQuery: url.Values{
			"start_date": []string{options.Start.Format(NasaDateFormat)},
			"end_date":   []string{options.End.Format(NasaDateFormat)},
			"api_key":    []string{nasa.apiKey},
		}.Encode(),
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url.String(), nil)
	if err != nil {
		return err
	}

	response, err := nasa.client.Do(request)
	if err != nil {
		return err
	}

	nasa.logger.Info("response received from nasa api", zap.Int("status_code", response.StatusCode), zap.String("rate_limit", response.Header.Get("X-RateLimit-Remaining")))

	if response.StatusCode == http.StatusTooManyRequests {
		// Treating 429 as 503, response lacks of X-Retry-After header
		// and X-RateLimit-Remaining is glitchy sometimes.
		return internal.Error(internal.ServiceUnavailable,
			internal.WithDescription("external service api is overloaded"))
	}

	if response.StatusCode == http.StatusForbidden {
		return internal.Error(internal.ServiceFailure,
			internal.WithDebug("nasa api restricted access for currently used api key"))
	}

	if response.StatusCode == http.StatusBadRequest {
		return internal.Error(internal.RequirementsUnmet,
			internal.WithDescription("given time range is not supported"))
	}

	if response.StatusCode != http.StatusOK {
		return internal.Error(internal.ServiceFailure,
			internal.WithDebug("nasa api returned unexpected status code %d", response.StatusCode))
	}

	defer func() {
		if err := response.Body.Close(); err != nil {
			nasa.logger.Warn("response body could not be closed", zap.Error(err))
		}
	}()

	var result NasaResult

	if err := json.NewDecoder(response.Body).Decode(&result); err != nil {
		return err
	}

	return result.WriteTo(destination)
}

func (nasa *NasaCollector) BufferSize(options service.CollectorOptions) int {
	return int(options.End.Sub(options.Start).Hours()/24) + 1
}

type NasaResult []struct {
	URL string `json:"url"`
}

func (result NasaResult) WriteTo(b []string) error {
	if len(b) < len(result) {
		return fmt.Errorf("buffer is too small expected at least %d but got %d", len(result), len(b))
	}
	for i, element := range result {
		b[i] = element.URL
	}
	return nil
}
