package collect

import (
	"context"
	"time"
	"url-collector/internal"
	"url-collector/pkg/service"

	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

// NOTE: this variable could be part of configuration
const ConcurrencyThreshold = 365

// ConcurrentCollector decorates underlying Collector with optional concurrent behaviour.
//
// If destination buffer is greater than ConcurrencyThreshold,
// then dataset is split in half and underlying collector.Collect is called concurrently.
//
// Given semaphore also restricts amount of concurrent Collect calls to it's capacity.
//
// This is the simplest strategy that isn't reckt by demo rate limits.
type ConcurrentCollector struct {
	logger    *zap.Logger
	semaphore *internal.Semaphore
	collector service.Collector
}

func NewConcurrentCollector(logger *zap.Logger, collector service.Collector, semaphore *internal.Semaphore) *ConcurrentCollector {
	return &ConcurrentCollector{
		semaphore: semaphore,
		collector: collector,
		logger:    logger,
	}
}

func (c *ConcurrentCollector) Collect(ctx context.Context, destination []string, options service.CollectorOptions) error {
	// if dealing with small dataset execute synchronously
	if len(destination) < ConcurrencyThreshold {
		return c.semaphore.Execute(ctx, func() error {
			return c.collector.Collect(ctx, destination, options)
		})
	}

	// otherwise split buffer in half and run collection concurrently
	offset := time.Duration(len(destination) / 2)

	group, ctx := errgroup.WithContext(ctx)

	// NOTE: In case of collectors with different resolution abstract specific interval
	group.Go(c.semaphore.WithAquire(ctx, func() error {
		return c.collector.Collect(ctx, destination[:offset+1], service.CollectorOptions{
			Start: options.Start,
			End:   options.Start.Add(offset * time.Hour * 24),
		})
	}))

	group.Go(c.semaphore.WithAquire(ctx, func() error {
		return c.collector.Collect(ctx, destination[offset+1:], service.CollectorOptions{
			Start: options.Start.Add((offset + 1) * time.Hour * 24),
			End:   options.End,
		})
	}))

	return group.Wait()
}

func (c *ConcurrentCollector) BufferSize(options service.CollectorOptions) int {
	return c.collector.BufferSize(options)
}
