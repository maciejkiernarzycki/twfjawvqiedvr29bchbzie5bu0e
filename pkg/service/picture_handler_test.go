package service_test

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"
	"url-collector/internal"
	"url-collector/mocks"
	"url-collector/pkg/service"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var timeStub = time.Date(2021, 6, 10, 0, 0, 0, 0, time.UTC)

var collectorOptionsStub = service.CollectorOptions{
	Start: timeStub,
	End:   timeStub,
}

func TestPictureHandler(t *testing.T) {

	t.Run("should not process requests with invalid form", func(t *testing.T) {
		controller := &mocks.Controller{}
		writer := &mocks.ErrorWriter{}
		recorder := httptest.NewRecorder()

		writer.On("WriteTo", recorder, mock.MatchedBy(MatchExpectedError(&internal.ExpectedError{
			Kind:        internal.RequirementsUnmet,
			Description: "invalid request query",
		})))

		service.PictureHandler(controller, writer)(recorder, &http.Request{
			URL: &url.URL{
				RawQuery: "%gh&%ij",
			},
		})
	})

	t.Run("should not process request with invalid query fields", func(t *testing.T) {
		controller := &mocks.Controller{}
		writer := &mocks.ErrorWriter{}
		recorder := httptest.NewRecorder()

		writer.
			On("WriteTo", recorder, mock.MatchedBy(MatchExpectedError(&internal.ExpectedError{
				Kind:        internal.RequirementsUnmet,
				Description: "missing required 'start_date' field",
			})))

		service.PictureHandler(controller, writer)(recorder, &http.Request{
			URL: &url.URL{
				RawQuery: "end_date=2021-10-06",
			},
		})
	})

	t.Run("should not return successful response on controller error", func(t *testing.T) {
		controller := &mocks.Controller{}
		writer := &mocks.ErrorWriter{}
		recorder := httptest.NewRecorder()

		errorStub := &internal.ExpectedError{
			Kind:        internal.ServiceFailure,
			Description: "writer test error",
		}

		controller.
			On("FetchByDateRange", mock.Anything, mock.MatchedBy(MatchCollectorOptions(collectorOptionsStub))).
			Return(nil, errorStub)

		writer.
			On("WriteTo", recorder, mock.MatchedBy(MatchExpectedError(errorStub)))

		service.PictureHandler(controller, writer)(recorder, &http.Request{
			URL: &url.URL{
				RawQuery: "start_date=2021-06-10&end_date=2021-06-10",
			},
		})
	})

	t.Run("should write successful response payload", func(t *testing.T) {
		controller := &mocks.Controller{}
		writer := &mocks.ErrorWriter{}
		recorder := httptest.NewRecorder()

		controller.
			On("FetchByDateRange", mock.Anything, mock.MatchedBy(MatchCollectorOptions(collectorOptionsStub))).
			Return([]string{"http://example.com/1", "http://example.com/2"}, nil)

		writer.
			AssertNotCalled(t, "WriteTo", mock.Anything, mock.Anything)

		service.PictureHandler(controller, writer)(recorder, &http.Request{
			URL: &url.URL{
				RawQuery: "start_date=2021-06-10&end_date=2021-06-10&",
			},
		})

		const expectedBody = "{\"urls\":[\"http://example.com/1\",\"http://example.com/2\"]}\n"

		assert.Equal(t, expectedBody, recorder.Body.String())
	})
}

func TestParsePictureRequestQuery(t *testing.T) {
	t.Run("should require start_date field", func(t *testing.T) {
		values := url.Values{
			"start_date": []string{},
			"end_date":   []string{},
		}

		query, err := service.ParsePictureRequestQuery(values)

		assert.Nil(t, query)
		assert.EqualError(t, err, "missing required 'start_date' field")
	})

	t.Run("should require end_date field", func(t *testing.T) {
		values := url.Values{
			"start_date": []string{"2021-06-10"},
			"end_date":   []string{},
		}

		query, err := service.ParsePictureRequestQuery(values)

		assert.Nil(t, query)
		assert.EqualError(t, err, "missing required 'end_date' field")
	})

	t.Run("should require start_date 2006-01-02 format", func(t *testing.T) {
		values := url.Values{
			"start_date": []string{"2021-14-10"},
			"end_date":   []string{"2021-06-10"},
		}

		query, err := service.ParsePictureRequestQuery(values)

		assert.Nil(t, query)
		assert.EqualError(t, err, "invalid 'start_date' field format")
	})

	t.Run("should require end_date 2006-01-02 format", func(t *testing.T) {
		values := url.Values{
			"start_date": []string{"2021-06-10"},
			"end_date":   []string{"2021-14-10"},
		}

		query, err := service.ParsePictureRequestQuery(values)

		assert.Nil(t, query)
		assert.EqualError(t, err, "invalid 'end_date' field format")
	})

	t.Run("should require start_date to be after end_date", func(t *testing.T) {
		values := url.Values{
			"start_date": []string{"2021-06-10"},
			"end_date":   []string{"2021-06-09"},
		}

		query, err := service.ParsePictureRequestQuery(values)

		assert.Nil(t, query)
		assert.EqualError(t, err, "invalid date range provided")
	})

	t.Run("should parse without error", func(t *testing.T) {
		values := url.Values{
			"start_date": []string{"2021-06-10"},
			"end_date":   []string{"2021-06-10"},
		}

		query, err := service.ParsePictureRequestQuery(values)

		assert.Equal(t, &service.PictureRequestQuery{
			StartDate: timeStub,
			EndDate:   timeStub,
		}, query)
		assert.Nil(t, err)
	})
}

func MatchCollectorOptions(expected service.CollectorOptions) func(service.CollectorOptions) bool {
	return func(options service.CollectorOptions) bool {
		return options.Start.Equal(expected.Start) && options.End.Equal(expected.End)
	}
}

func MatchExpectedError(e *internal.ExpectedError) func(err error) bool {
	return func(err error) bool {
		if err == nil {
			return false
		}

		expected, ok := err.(*internal.ExpectedError)
		if !ok {
			return false
		}

		if expected.Kind != e.Kind || expected.Description != e.Description || expected.Debug != e.Debug {
			return false
		}

		return true
	}
}
