package service

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"time"
	"url-collector/internal"
)

type Controller interface {
	FetchByDateRange(ctx context.Context, options CollectorOptions) ([]string, error)
}

type ErrorWriter interface {
	WriteTo(writer http.ResponseWriter, err error)
}

func PictureHandler(controller Controller, writer ErrorWriter) http.HandlerFunc {
	return func(response http.ResponseWriter, request *http.Request) {
		if err := request.ParseForm(); err != nil {
			writer.WriteTo(response, internal.Error(internal.RequirementsUnmet, internal.WithDescription("invalid request query")))
			return
		}

		query, err := ParsePictureRequestQuery(request.Form)
		if err != nil {
			writer.WriteTo(response, err)
			return
		}

		urls, err := controller.FetchByDateRange(request.Context(), CollectorOptions{
			Start: query.StartDate,
			End:   query.EndDate,
		})
		if err != nil {
			writer.WriteTo(response, err)
			return
		}

		json.NewEncoder(response).Encode(&PicureResponsePayload{
			URLS: urls,
		})
	}
}

type PictureRequestQuery struct {
	StartDate time.Time
	EndDate   time.Time
}

type PicureResponsePayload struct {
	URLS []string `json:"urls"`
}

func ParsePictureRequestQuery(values url.Values) (*PictureRequestQuery, error) {
	// NOTE: inconviniet but gorilla/schema decoder produces ugly messages
	//       could search for replacement.

	const format = "2006-01-02"
	startDate, endDate := values.Get("start_date"), values.Get("end_date")

	if startDate == "" {
		return nil, internal.Error(internal.RequirementsUnmet, internal.WithDescription("missing required 'start_date' field"))
	}

	if endDate == "" {
		return nil, internal.Error(internal.RequirementsUnmet, internal.WithDescription("missing required 'end_date' field"))
	}

	start, err := time.Parse(format, startDate)
	if err != nil {
		return nil, internal.Error(internal.RequirementsUnmet, internal.WithDescription("invalid 'start_date' field format"))
	}

	end, err := time.Parse(format, endDate)
	if err != nil {
		return nil, internal.Error(internal.RequirementsUnmet, internal.WithDescription("invalid 'end_date' field format"))
	}

	if end.Before(start) {
		return nil, internal.Error(internal.RequirementsUnmet, internal.WithDescription("invalid date range provided"))
	}

	return &PictureRequestQuery{
		StartDate: start,
		EndDate:   end,
	}, nil
}
