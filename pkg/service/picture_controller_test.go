package service_test

import (
	"context"
	"errors"
	"testing"
	"url-collector/mocks"
	"url-collector/pkg/service"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestPictureController_FetchByDateRange(t *testing.T) {
	t.Run("should propagate error from collector", func(t *testing.T) {
		ctx := context.Background()
		collector := &mocks.Collector{}

		collector.
			On("BufferSize", mock.MatchedBy(MatchCollectorOptions(collectorOptionsStub))).
			Return(1)

		collector.
			On("Collect", ctx, []string{""}, collectorOptionsStub).
			Return(errors.New("test error"))

		controller := service.NewPictureController(collector)

		result, err := controller.FetchByDateRange(ctx, collectorOptionsStub)

		assert.Nil(t, result)
		assert.EqualError(t, err, "test error")
	})

	t.Run("should return allocated and collected buffer", func(t *testing.T) {
		ctx := context.Background()
		collector := &mocks.Collector{}

		collector.
			On("BufferSize", mock.MatchedBy(MatchCollectorOptions(collectorOptionsStub))).
			Return(1)

		collector.
			On("Collect", ctx, []string{""}, collectorOptionsStub).
			Return(nil)

		controller := service.NewPictureController(collector)

		result, err := controller.FetchByDateRange(ctx, collectorOptionsStub)

		assert.Nil(t, err)
		assert.Equal(t, result, []string{""})
	})
}
