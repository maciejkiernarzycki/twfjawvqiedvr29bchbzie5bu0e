package service

import (
	"context"
	"time"
)

type CollectorOptions struct {
	Start time.Time
	End   time.Time
}

type Collector interface {
	BufferSize(options CollectorOptions) int
	Collect(ctx context.Context, destination []string, options CollectorOptions) error
}

type PictureController struct {
	collector Collector
}

func NewPictureController(collector Collector) *PictureController {
	return &PictureController{
		collector: collector,
	}
}

func (controller *PictureController) FetchByDateRange(ctx context.Context, options CollectorOptions) ([]string, error) {
	buffer := make([]string, controller.collector.BufferSize(options))

	if err := controller.collector.Collect(ctx, buffer, options); err != nil {
		return nil, err
	}

	return buffer, nil
}
