module url-collector

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/sethvargo/go-envconfig v0.3.5
	github.com/stretchr/testify v1.7.0
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/tools v0.1.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
