# url-collector

```sh
# build image
docker build -t url-collector:latest .

# run container 
docker run --env API_KEY=DEMO_KEY --env CONCURRENT_REQUESTS=5 --env PORT=8080 -p 127.0.0.1:8080:8080 url-collector:latest /go/bin/url-collector
```