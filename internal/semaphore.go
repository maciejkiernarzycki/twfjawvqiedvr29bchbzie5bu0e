package internal

import (
	"context"

	"golang.org/x/sync/semaphore"
)

type Semaphore struct {
	semaphore *semaphore.Weighted
}

func NewSemaphore(count int64) *Semaphore {
	return &Semaphore{
		semaphore: semaphore.NewWeighted(count),
	}
}

func (s *Semaphore) Execute(ctx context.Context, fn func() error) error {
	if err := s.semaphore.Acquire(ctx, 1); err != nil {
		return err
	}
	defer s.semaphore.Release(1)

	return fn()
}

func (s *Semaphore) WithAquire(ctx context.Context, fn func() error) func() error {
	return func() error {
		return s.Execute(ctx, fn)
	}
}
