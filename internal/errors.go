package internal

import (
	"encoding/json"
	"fmt"
	"net/http"

	"go.uber.org/zap"
)

type ErrorWriter struct {
	logger *zap.Logger
}

func NewErrorWriter(logger *zap.Logger) *ErrorWriter {
	return &ErrorWriter{
		logger: logger,
	}
}

func (w *ErrorWriter) WriteTo(response http.ResponseWriter, err error) {
	expected, ok := err.(*ExpectedError)
	if !ok {
		w.logger.Error("server responded with unexpected error", zap.Error(err))
		http.Error(response, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	code := KindToStatusCode(expected.Kind)

	if code == http.StatusInternalServerError {
		w.logger.Error("server responded with unexpected error", zap.Error(err))
		http.Error(response, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.logger.Info("server responded with error", zap.Error(err))

	response.WriteHeader(code)
	json.NewEncoder(response).Encode(expected)
}

type ExpectedError struct {
	Kind        ErrorKind
	Description string
	Debug       string
}

func Error(kind ErrorKind, options ...ErrorOption) error {
	err := &ExpectedError{
		Kind: kind,
	}

	for _, option := range options {
		option(err)
	}

	return err
}

func (e *ExpectedError) Error() string {
	if e.Description != "" && e.Debug != "" {
		return fmt.Sprintf("%s: %s", e.Description, e.Debug)
	}

	if e.Description != "" {
		return e.Description
	}

	return e.Debug
}

func (e *ExpectedError) MarshalJSON() ([]byte, error) {
	type payload struct {
		Error string `json:"error"`
	}

	if e.Description == "" {
		return json.Marshal(&payload{
			Error: "something went wrong",
		})
	}

	return json.Marshal(&payload{
		Error: e.Description,
	})
}

type ErrorOption func(err *ExpectedError)

func WithDescription(format string, args ...interface{}) ErrorOption {
	return func(err *ExpectedError) {
		err.Description = fmt.Sprintf(format, args...)
	}
}

func WithDebug(format string, args ...interface{}) ErrorOption {
	return func(err *ExpectedError) {
		err.Debug = fmt.Sprintf(format, args...)
	}
}

type ErrorKind int

const (
	RequirementsUnmet ErrorKind = iota
	ResourceMissing
	ServiceOverload
	ServiceUnavailable
	ServiceFailure
)

func KindToStatusCode(kind ErrorKind) int {
	switch kind {
	case RequirementsUnmet:
		return http.StatusBadRequest
	case ResourceMissing:
		return http.StatusNotFound
	case ServiceOverload:
		return http.StatusTooManyRequests
	case ServiceUnavailable:
		return http.StatusServiceUnavailable
	default:
		return http.StatusInternalServerError
	}
}
