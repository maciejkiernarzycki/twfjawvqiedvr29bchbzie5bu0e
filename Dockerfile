FROM golang:1.15.8-alpine3.13 as builder

ENV GO111MODULE on

WORKDIR $GOPATH/src/url-collector

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

ARG APP="./..."

RUN CGO_ENABLED=0 go install -tags static ./...

FROM alpine:3.13

ENV GOPATH="/go"
ENV PATH="${GOPATH}/bin:${PATH}"

COPY --from=builder /go/bin /go/bin
